import React, { ReactElement, useState, useCallback } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Login from './pages/Login';
import { getToken, resetToken } from './api/auth';
import UserProvider from './components/UserProvider';
import { Signup } from './pages/Signup';
import { Dashboard } from './pages/Dashboard';

interface PrivateRouteProps {
    component: React.ComponentType<any>;
    logged: boolean;
    [key: string]: unknown;
}

const PrivateRoute = ({ component: Component, logged, ...rest }: PrivateRouteProps): ReactElement => (
    <Route
        {...rest}
        render={(props): ReactElement =>
            logged === true ? (
                <Component {...props} />
            ) : (
                <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
            )
        }
    />
);
const Routes = (): ReactElement => {
    const token = getToken();
    const [logged, setLogged] = useState<boolean>(!!token);
    const handleLogin = useCallback(() => setLogged(true), []);
    const handleLogout = useCallback(() => {
        setLogged(false);
        resetToken();
    }, []);

    return (
        <UserProvider onLogout={handleLogout} logged={logged}>
            <Router>
                <Switch>
                    <PrivateRoute path="/dashboard" component={Dashboard} logged={logged} />
                    <Route path='/signup' render={(props): ReactElement => <Signup {...props} onSuccess={handleLogin} />} />
                    <Route render={(props): ReactElement => <Login {...props} onSuccess={handleLogin} logout={handleLogout} />} />
                </Switch>
            </Router>
        </UserProvider>
    );
};

export default Routes;

import React, { ReactElement } from 'react';
import { Link, Typography } from '@material-ui/core';

export const Copyright = (): ReactElement => (
    <Typography variant="body2" color="textSecondary" align="center">
        {'Copyright © '}
        <Link color="inherit" href="https://material-ui.com/">
            Marian Alexandru Alecu
        </Link>{' '}
        {new Date().getFullYear()}
        {'.'}
    </Typography>
);

import React, { useState, ChangeEvent, ReactElement } from 'react';
import { TextField, MenuItem } from '@material-ui/core';

type SelectProps = {
    label: string;
    helperText?: string;
    options: Record<string, string>;
    onChange?: (value: string) => void;
    defaultValue: string;
};
export const Select = (props: SelectProps): ReactElement => {
    const { label, helperText = '', options, onChange, defaultValue } = props;
    const [value, setValue] = useState<string>(defaultValue);
    const onChangeLocal = (event: ChangeEvent<HTMLInputElement>): void => {
        setValue(event.target.value);

        onChange(event.target.value);
    };

    return (
        <TextField
            select
            style={{ width: '150px' }}
            variant="outlined"
            label={label}
            helperText={helperText}
            onChange={onChangeLocal}
            value={value}
        >
            {Object.keys(options).map(key => (
                <MenuItem key={key} value={key}>
                    {options[key]}
                </MenuItem>
            ))}
        </TextField>
    );
};

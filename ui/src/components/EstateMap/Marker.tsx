import React from 'react';
import { makeStyles } from '@material-ui/core';

const markerStyles = makeStyles(() => ({
    root: {
        width: 100,
        backgroundColor: 'white',
        boxShadow: '0 2px 7px 1px rgba(0, 0, 0, 0.3)',
        padding: 2,
    },
}));

export const Marker = (props: { lat: number; lng: number; children: any }) => {
    const classes = markerStyles({});

    return <div className={classes.root}>{props.children}</div>;
};

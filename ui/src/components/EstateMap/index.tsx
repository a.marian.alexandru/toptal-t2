import React, { useState, useEffect, useRef } from 'react';
import GoogleMapReact from 'google-map-react';
import { makeStyles, TextField, Box, Button } from '@material-ui/core';
import { Estate } from '../../types';
import { Marker } from './Marker';

const apiKey = 'AIzaSyCO0k1LOexglNNkSV8Y-IxcBWOAcFH2hjU';

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative',
        height: 400,
        width: '100%',
        marginBottom: theme.spacing(4),
    },
    geocodeField: {
        background: 'white',
        position: 'absolute',
        left: theme.spacing(2),
        top: theme.spacing(2),
        zIndex: 99,
        padding: '0 5px',
    },
}));

const geocode = (locationSearch: string) => {
    return new Promise<google.maps.LatLngLiteral>((resolve, reject) => {
        const geocoder = new google.maps.Geocoder();

        geocoder.geocode({ address: locationSearch }, (results, status) => {
            if (status === 'OK') {
                const { lat, lng } = results[0].geometry.location;
                const resultLocation = { lat: lat(), lng: lng() };

                resolve(resultLocation);
            } else {
                reject(status);
            }
        });
    });
};

const getCoordinates = (estate: Estate): google.maps.LatLngLiteral | null => {
    if (!estate || !estate.lat) return null;

    return {
        lat: estate.lat,
        lng: estate.lon,
    };
};

type Props = {
    locations: Estate[];
    onLocationChange?: (location: google.maps.LatLngLiteral) => void;
};

export const EstateMap = (props: Props) => {
    const { onLocationChange, locations } = props;

    if (!locations.length) return <div>Loading...</div>;

    const classes = useStyles({});
    const [mapCenter, setMapCenter] = useState<google.maps.LatLngLiteral>(
        getCoordinates(locations[0])
    );
    const [locationSearch, setLocationSearch] = useState<string>('');

    const search = async () => {
        try {
            const result = await geocode(locationSearch);

            if (onLocationChange) {
                onLocationChange(result);
            }

            setMapCenter(result);
        } catch (err) {
            alert(`Geocode was not successful: ${err}`);
        }
    };

    return (
        <div className={classes.root}>
            <Box className={classes.geocodeField}>
                <TextField
                    size="small"
                    onChange={(event) => setLocationSearch(event.target.value)}
                />
                <Button onClick={search}>Search</Button>
            </Box>
            <GoogleMapReact
                defaultCenter={{ lat: 46.7722, lng: 23.6052 }}
                bootstrapURLKeys={{ key: apiKey }}
                center={mapCenter}
                yesIWantToUseGoogleMapApiInternals
                defaultZoom={12}
            >
                {locations.map((el) => (
                    <Marker key={el.id} lat={el.lat} lng={el.lon}>
                        {el.name} - ${el.price}
                    </Marker>
                ))}
            </GoogleMapReact>
        </div>
    );
};

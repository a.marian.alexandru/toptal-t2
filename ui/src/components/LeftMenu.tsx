import React, { useContext } from 'react';
import { Link, Route, RouteComponentProps } from 'react-router-dom';
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { Dashboard, People } from '@material-ui/icons';
import { UserContext } from '../context/user';

const isSelected = (props: RouteComponentProps, to: string): boolean =>
    props.location.pathname === to;
const dashboardUrl = '/dashboard';

export const LeftMenu = () => {
    const user = useContext(UserContext);

    return (
        <Route>
            {(props: RouteComponentProps): React.ReactElement => (
                <>
                    <ListItem
                        button
                        selected={isSelected(props, dashboardUrl + '/estates')}
                        component={Link}
                        to={dashboardUrl + '/estates'}
                    >
                        <ListItemIcon>
                            <Dashboard />
                        </ListItemIcon>
                        <ListItemText primary="Apartments" />
                    </ListItem>

                    {user.role === 'ADMIN' && (
                        <ListItem
                            button
                            selected={isSelected(
                                props,
                                dashboardUrl + '/users'
                            )}
                            component={Link}
                            to={dashboardUrl + '/users'}
                        >
                            <ListItemIcon>
                                <People />
                            </ListItemIcon>
                            <ListItemText primary="Users" />
                        </ListItem>
                    )}
                </>
            )}
        </Route>
    );
};

import React, { ReactElement, Ref, FormEvent } from 'react';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { makeStyles } from '@material-ui/core/styles';
import {
    Avatar,
    Button,
    CssBaseline,
    TextField,
    Box,
    Typography,
    Container,
    Grid,
    Link,
} from '@material-ui/core';
import { SchemaErrors } from '../../types/SchemaErrors';
import { Copyright } from '../Copyright';

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

type Props = {
    title: string;
    handleSubmit: (event: FormEvent<HTMLFormElement>) => void;
    errors: SchemaErrors;
    emailRef: Ref<ReactElement>;
    passwordRef: Ref<ReactElement>;
};

export const LoginTemplate = (props: Props): ReactElement => {
    const { handleSubmit, errors, emailRef, passwordRef, title } = props;
    const classes = useStyles({});

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    {title}
                </Typography>
                <form className={classes.form} onSubmit={handleSubmit}>
                    <TextField
                        autoComplete="email"
                        autoFocus
                        error={!!errors.email}
                        fullWidth
                        helperText={'email' in errors && errors.email.message}
                        id="email"
                        inputRef={emailRef}
                        label="Email"
                        margin="normal"
                        name="email"
                        required
                        variant="outlined"
                    />
                    <TextField
                        autoComplete="current-password"
                        error={!!errors.password}
                        fullWidth
                        helperText={'password' in errors && errors.password.message}
                        id="password"
                        inputRef={passwordRef}
                        label="Parola"
                        margin="normal"
                        name="password"
                        required
                        type="password"
                        variant="outlined"
                    />
                    <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit}>
                        Submit
                    </Button>
                    <Grid container>
                        <Grid item>
                            <Link href="/signup" variant="body2">
                                No account? Signup!
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
            <Box mt={8}>
                <Copyright />
            </Box>
        </Container>
    );
};

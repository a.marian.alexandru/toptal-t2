import React, { FormEvent, Ref, ReactElement } from 'react';
import { SchemaErrors, EstateState, User, UserRole } from '../../types';
import {
    TextField,
    Button,
    Box,
    makeStyles,
    MenuItem,
} from '@material-ui/core';
import { Paper } from '../Paper';
import { Controller, Control } from 'react-hook-form';

const useStyles = makeStyles((theme) => ({
    fieldsContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'space-between',
        '& > *': {
            margin: theme.spacing(2),
            minWidth: 200,
        },
    },
    buttonsContainer: {
        display: 'flex',
        '& > *': {
            margin: theme.spacing(2),
            minWidth: 200,
        },
    },
}));

type Props = {
    handleSubmit: (event: FormEvent<HTMLFormElement>) => void;
    errors: SchemaErrors;
    register: Ref<ReactElement>;
    control: Control;
    goBack: () => void;
    onDelete: () => void;
};

const roleOptions: Record<UserRole, string> = {
    USER: 'User',
    REALTOR: 'Realtor',
    ADMIN: 'Admin',
};

export const UserDetailsTemplate = (props: Props) => {
    const { handleSubmit, errors, register, control } = props;
    const classes = useStyles({});
    const defaultTextProps: Record<
        string,
        Ref<ReactElement> | string | boolean
    > = {
        defaultValue: '_',
        inputRef: register,
        margin: 'normal',
        variant: 'outlined',
    };
    const getError = (field: string) =>
        field in errors && errors[field].message;

    return (
        <Paper>
            <form onSubmit={handleSubmit}>
                <Box className={classes.fieldsContainer}>
                    <TextField
                        {...defaultTextProps}
                        error={!!errors.email}
                        helperText={getError('email')}
                        label="Email"
                        name="email"
                        type="email"
                    />
                    <TextField
                        {...defaultTextProps}
                        error={!!errors.password}
                        helperText={getError('password')}
                        label="Password"
                        name="password"
                        required
                        type="password"
                    />
                    <Controller
                        name="role"
                        control={control}
                        defaultValue="USER"
                        as={
                            <TextField
                                {...defaultTextProps}
                                error={!!errors.role}
                                helperText={getError('role')}
                                label="Role"
                                required
                                select
                            >
                                {Object.keys(roleOptions).map(
                                    (key: UserRole) => (
                                        <MenuItem key={key} value={key}>
                                            {roleOptions[key]}
                                        </MenuItem>
                                    )
                                )}
                            </TextField>
                        }
                    />
                </Box>

                <Box className={classes.buttonsContainer}>
                    <Button type="submit" variant="contained" color="primary">
                        Submit
                    </Button>
                    <Button
                        variant="contained"
                        color="default"
                        onClick={props.goBack}
                    >
                        Go Back
                    </Button>
                    <Button
                        onClick={props.onDelete}
                        variant="contained"
                        color="secondary"
                    >
                        Delete
                    </Button>
                </Box>
            </form>
        </Paper>
    );
};

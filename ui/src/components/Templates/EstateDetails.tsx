import React, { FormEvent, Ref, ReactElement } from 'react';
import { SchemaErrors, EstateState, User } from '../../types';
import {
    TextField,
    Button,
    Box,
    makeStyles,
    MenuItem,
} from '@material-ui/core';
import { Controller, Control } from 'react-hook-form';

const useStyles = makeStyles((theme) => ({
    fieldsContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'space-between',
        '& > *': {
            margin: theme.spacing(2),
            minWidth: 200,
        },
    },
    buttonsContainer: {
        display: 'flex',
        '& > *': {
            margin: theme.spacing(2),
            minWidth: 200,
        },
    },
}));

type Props = {
    handleSubmit: (event: FormEvent<HTMLFormElement>) => void;
    errors: SchemaErrors;
    register: Ref<ReactElement>;
    control: Control;
    goBack: () => void;
    onDelete: () => void;
    users: User[];
};

const stateOptions: Record<EstateState, string> = {
    AVAILABLE: 'Available',
    RENTED: 'Rented',
};

export const EstateDetailsTemplate = (props: Props) => {
    const { handleSubmit, errors, register, control, users } = props;
    const classes = useStyles({});
    const defaultTextProps: Record<
        string,
        Ref<ReactElement> | string | boolean
    > = {
        defaultValue: ' ',
        inputRef: register,
        margin: 'normal',
        variant: 'outlined',
    };
    const getError = (field: string) =>
        field in errors && errors[field].message;

    return (
        <form onSubmit={handleSubmit}>
            <Box className={classes.fieldsContainer}>
                <TextField
                    {...defaultTextProps}
                    error={!!errors.lat}
                    helperText={getError('lat')}
                    label="Latitude"
                    name="lat"
                    required
                    type="number"
                    defaultValue={0}
                    inputProps={{ step: '.0000001' }}
                />
                <TextField
                    {...defaultTextProps}
                    error={!!errors.lon}
                    helperText={getError('lon')}
                    label="Longitude"
                    name="lon"
                    required
                    type="number"
                    defaultValue={0}
                    inputProps={{ step: '.0000001' }}
                />
                <Controller
                    name="realtor"
                    control={control}
                    defaultValue="0"
                    as={
                        <TextField
                            {...defaultTextProps}
                            error={!!errors.realtor}
                            helperText={getError('realtor')}
                            label="Realtor"
                            required
                            select
                        >
                            {users.map((user) => (
                                <MenuItem key={user.id} value={user.id}>
                                    {user.email}
                                </MenuItem>
                            ))}
                        </TextField>
                    }
                />
                <TextField
                    {...defaultTextProps}
                    error={!!errors.name}
                    helperText={getError('name')}
                    label="Name"
                    name="name"
                    required
                />
                <TextField
                    {...defaultTextProps}
                    error={!!errors.description}
                    helperText={getError('description')}
                    label="Description"
                    name="description"
                    required
                    multiline
                    rows={10}
                    fullWidth
                />
                <TextField
                    {...defaultTextProps}
                    error={!!errors.rooms}
                    helperText={getError('rooms')}
                    label="Rooms"
                    name="rooms"
                    required
                    type="number"
                    defaultValue={0}
                />
                <TextField
                    {...defaultTextProps}
                    error={!!errors.price}
                    helperText={getError('price')}
                    label="Price ($)"
                    name="price"
                    required
                    type="number"
                    inputProps={{ step: '.01' }}
                    defaultValue={0}
                />
                <TextField
                    {...defaultTextProps}
                    error={!!errors.size}
                    helperText={getError('size')}
                    label="Size"
                    name="size"
                    required
                    type="number"
                    defaultValue={0}
                    inputProps={{ step: '.1' }}
                />
                <Controller
                    name="state"
                    control={control}
                    defaultValue="AVAILABLE"
                    as={
                        <TextField
                            margin="normal"
                            variant="outlined"
                            error={!!errors.state}
                            helperText={getError('state')}
                            label="Status"
                            required
                            select
                        >
                            {Object.keys(stateOptions).map(
                                (key: EstateState) => (
                                    <MenuItem key={key} value={key}>
                                        {stateOptions[key]}
                                    </MenuItem>
                                )
                            )}
                        </TextField>
                    }
                />
            </Box>

            <Box className={classes.buttonsContainer}>
                <Button type="submit" variant="contained" color="primary">
                    Submit
                </Button>
                <Button
                    variant="contained"
                    color="default"
                    onClick={props.goBack}
                >
                    Go Back
                </Button>
                <Button
                    onClick={props.onDelete}
                    variant="contained"
                    color="secondary"
                >
                    Delete
                </Button>
            </Box>
        </form>
    );
};

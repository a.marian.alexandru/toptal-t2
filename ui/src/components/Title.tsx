import React from 'react';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core';

const TitleCore = (props: { children: unknown; classes: { [key: string]: string } }): React.ReactElement => {
    return (
        <Typography component="h2" variant="h6" color="primary" gutterBottom classes={props.classes}>
            {props.children}
        </Typography>
    );
};

export const Title = withStyles(theme => ({
    root: {
        marginBottom: (props: { marginBotton?: string | number }): string | number =>
            props.marginBotton || theme.spacing(2),
    },
}))(TitleCore);

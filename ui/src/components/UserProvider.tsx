import React, { useEffect, useState } from 'react';
import { UserContext, emptyUser } from '../context/user';
import { getToken, fetchLoggedUser } from '../api/auth';
import { User } from '../types';

type Props = { children: unknown; onLogout: () => void; logged: boolean };

const UserProvider = (props: Props): React.ReactElement => {
    const token = getToken();
    const [user, setUser] = useState<User>(emptyUser);

    useEffect(() => {
        (async (): Promise<void> => {
            try {
                if (token && props.logged) setUser(await fetchLoggedUser());
            } catch (err) {
                props.onLogout();
            }
        })();
    }, [props.logged]);

    return (
        <UserContext.Provider value={user}>
            {props.children}
        </UserContext.Provider>
    );
};

export default UserProvider;

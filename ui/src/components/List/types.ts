// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type Row = Record<string, any>;
export type EventHandler<T extends Row> = (row: T) => void;

import { SyntheticEvent } from 'react';

export const preventDefaultNStop = (event: SyntheticEvent): void => {
    event.preventDefault();
    event.stopPropagation();
};

import { ValidationError } from 'yup';

export interface SchemaErrors {
    [key: string]: ValidationError;
}

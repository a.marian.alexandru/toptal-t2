export type EstateState = 'AVAILABLE' | 'RENTED';

export interface Estate {
    id: number;
    add_date: string;
    realtor: number;
    name: string;
    description: string;
    size: number;
    rooms: number;
    price: number;
    lat: number;
    lon: number;
    state: EstateState;
}

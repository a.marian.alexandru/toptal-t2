export type UserRole = 'ADMIN' | 'REALTOR' | 'USER';

export interface User {
    id: number;
    email: string;
    password: string;
    role: UserRole;
}

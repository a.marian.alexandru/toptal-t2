import React, { useEffect, useState, useCallback, useContext } from 'react';
import * as api from '../../api/estate';
import * as userApi from '../../api/user';
import { Paper } from '../../components/Paper';
import { List } from '../../components/List';
import { Estate, User } from '../../types';
import { AvailableFilter } from '../../api/estate';
import { EstateFilters } from './Filters';
import { debounce, Button } from '@material-ui/core';
import { RouteComponentProps } from 'react-router-dom';
import { UserContext } from '../../context/user';
import { EstateMap } from '../../components/EstateMap';

type EstateWithRealtor = Estate & { realtorName: string };
const header: Partial<Record<keyof EstateWithRealtor, string>> = {
    add_date: 'Post date',
    name: 'Name',
    rooms: 'No. rooms',
    size: 'Size (mp)',
    description: 'Description',
    price: 'Price ($)',
    realtorName: 'Realtor',
    state: 'Status',
};
const parseDate = (date: string): string => new Date(date).toLocaleString();

export const EstateList = (props: RouteComponentProps): React.ReactElement => {
    const user = useContext(UserContext);
    const [list, setList] = useState<Array<EstateWithRealtor>>([]);
    const [realtors, setRealtors] = useState<Record<number, User>>({});
    const [filter, setFilter] = useState<AvailableFilter>({});

    useEffect(() => {
        (async (): Promise<void> => {
            const list = await api.get(filter);
            const parsed: EstateWithRealtor[] = list.map((el) => {
                const realtor = realtors[el.realtor] as User | undefined;

                return {
                    ...el,
                    add_date: parseDate(el.add_date),
                    realtorName: realtor?.email || '',
                };
            });

            setList(parsed);
        })();
    }, [filter]);

    useEffect(() => {
        (async (): Promise<void> => {
            const missingRealtorsIds = list
                .map((el) => el.realtor)
                .filter((id) => !realtors[id]);
            const unique = [...new Set(missingRealtorsIds)];

            if (!unique.length) return;

            const missingRealtors = await Promise.all(
                unique.map(userApi.getById)
            );
            const realtorsMap = missingRealtors.reduce(
                (acc, el) => ({ ...acc, [el.id]: el }),
                {}
            );
            const allRealtors: Record<number, User> = {
                ...realtors,
                ...realtorsMap,
            };

            setRealtors(allRealtors);

            setList(
                list.map((el) => ({
                    ...el,
                    realtorName: allRealtors[el.realtor]?.email,
                }))
            );
        })();
    }, [list]);

    const renderFilters = useCallback(
        () => <EstateFilters onChange={debounce(setFilter)} />,
        []
    );
    const renderActions = useCallback(
        () => (
            <Button
                color="primary"
                variant="outlined"
                onClick={() => props.history.push('/dashboard/estate/new')}
            >
                Create
            </Button>
        ),
        []
    );

    const redirectToDetails = (el: Estate) => {
        if (user.role !== 'USER') {
            props.history.push('/dashboard/estate/' + el.id);
        }
    };

    return (
        <Paper>
            <EstateMap locations={list} />
            <List
                onRowClick={redirectToDetails}
                title="Apartments"
                renderFilters={renderFilters}
                renderActions={renderActions}
                header={header}
                rows={list}
            />
        </Paper>
    );
};

import React, {
    useCallback,
    ChangeEvent,
    useRef,
} from 'react';
import { Select } from '../../components/Select';
import { AvailableFilter } from '../../api/estate';
import { TextField, Box, makeStyles } from '@material-ui/core';
import { EstateState } from '../../types';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
        },
    },
    field: {
        width: 110,
    },
}));

const statusOptions = {
    default: 'Show all',
    AVAILABLE: 'Available',
    RENTED: 'Rented',
};
const defaultStatusFilter = 'default';

type Props = {
    onChange: (filters: AvailableFilter) => void;
};

const isEvent = (value: any): value is ChangeEvent<HTMLInputElement> =>
    typeof value === 'object' && 'target' in value;
const isEstateState = (value: any): value is EstateState =>
    value === 'AVAILABLE' || value === 'RENTED';
const getPartialFilter = (
    field: keyof AvailableFilter,
    event: string | ChangeEvent
) => {
    const value = isEvent(event) ? event.target.value : (event as string);

    if (field === 'state') {
        if (isEstateState(value)) {
            return { state: value };
        }
    } else {
        if (value === '' || isNaN(Number(value))) return null;

        return { [field]: parseInt(value) };
    }
};

export const EstateFilters = (props: Props): React.ReactElement => {
    const classes = useStyles({});
    const filter = useRef<AvailableFilter>({});
    const handleChange = useCallback(
        (field: keyof AvailableFilter) => (event: string | ChangeEvent) => {
            const fieldFilter = getPartialFilter(field, event);

            if (!fieldFilter) {
                delete filter.current[field];
            } else {
                filter.current = {
                    ...filter.current,
                    ...fieldFilter,
                };
            }

            // always create new object
            props.onChange({ ...filter.current });
        },
        []
    );

    return (
        <Box display="flex" className={classes.root} alignItems='center'>
            <TextField
                size="small"
                type="number"
                variant="outlined"
                label="From size"
                className={classes.field}
                onChange={handleChange('sizeFrom')}
            />
            {' - '}
            <TextField
                size="small"
                type="number"
                variant="outlined"
                label="To size"
                className={classes.field}
                onChange={handleChange('sizeTo')}
            />
            <TextField
                size="small"
                type="number"
                variant="outlined"
                label="From price"
                className={classes.field}
                onChange={handleChange('priceFrom')}
            />
            {' - '}
            <TextField
                size="small"
                type="number"
                variant="outlined"
                label="To price"
                className={classes.field}
                onChange={handleChange('priceTo')}
            />
            <TextField
                size="small"
                type="number"
                variant="outlined"
                label="From rooms"
                className={classes.field}
                onChange={handleChange('roomsFrom')}
            />
            {' - '}
            <TextField
                size="small"
                type="number"
                variant="outlined"
                label="To rooms"
                className={classes.field}
                onChange={handleChange('roomsTo')}
            />
            <Select
                label="Status"
                options={statusOptions}
                onChange={handleChange('state')}
                defaultValue={defaultStatusFilter}
            />
        </Box>
    );
};

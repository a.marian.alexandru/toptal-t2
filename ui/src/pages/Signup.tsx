import React, { SyntheticEvent, useCallback } from 'react';
import { useForm } from 'react-hook-form';
import { LoginTemplate } from '../components/Templates/LoginTemplate';
import { SchemaErrors } from '../types/SchemaErrors';
import { setToken, signup } from '../api/auth';
import { RouteComponentProps } from 'react-router-dom';
import { LoginSchema, LoginValues } from '../forms/login';

interface Props extends RouteComponentProps {
    onSuccess: () => void;
}

export const Signup = (props: Props): React.ReactElement => {
    const { register, handleSubmit, errors, setError } = useForm({
        validationSchema: LoginSchema,
    });
    const setTokenAndRedirect = useCallback((token: string): void => {
        setToken(token);
        props.onSuccess();
        props.history.push('/dashboard');
    }, []);
    const onSubmit = async (
        data: LoginValues,
        e: SyntheticEvent
    ): Promise<void> => {
        e.preventDefault();

        try {
            const token = await signup(data.email, data.password);

            setTokenAndRedirect(token);
        } catch (err) {
            const msg = err?.response?.data || 'Signup failed.';

            setError('password', 'invalidLogin', msg);
        }
    };

    return (
        <LoginTemplate
            title="Signup"
            handleSubmit={handleSubmit(onSubmit)}
            errors={errors as SchemaErrors}
            emailRef={register}
            passwordRef={register}
        />
    );
};

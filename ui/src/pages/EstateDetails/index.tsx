import React, { useCallback, useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { RouteComponentProps } from 'react-router-dom';
import { EstateDetailsTemplate } from '../../components/Templates/EstateDetails';
import { SchemaErrors, Estate, User } from '../../types';
import * as api from '../../api/estate';
import { getRealtors } from '../../api/user';
import { Paper } from '../../components/Paper';
import { EstateMap } from '../../components/EstateMap';

export const EstateDetails = (
    props: RouteComponentProps<{ id: string }>
): React.ReactElement => {
    const { id } = props.match.params;
    const {
        control,
        register,
        handleSubmit,
        errors,
        setError,
        setValue,
    } = useForm({});
    const [estate, setEstate] = useState<Estate>({} as Estate);
    const onSubmit = useCallback(
        (estate: Estate) => {
            (async () => {
                if (id === 'new') {
                    const response = await api.create(estate);

                    props.history.push('/dashboard/estate/' + response.id);
                } else {
                    await api.update({ ...estate, id: parseInt(id) });
                }
            })();
        },
        [id]
    );
    const [realtors, setRealtors] = useState<User[]>([]);
    const goBack = () => props.history.push('/dashboard');

    const deleteEstate = async () => {
        if (Number.isInteger(Number(id))) {
            await api.remove(parseInt(id));
        }

        goBack();
    };

    useEffect(() => {
        (async () => {
            setRealtors(await getRealtors());
        })();
    }, []);

    useEffect(() => {
        (async () => {
            if (id === 'new') return;

            setEstate(await api.getById(parseInt(id)));
        })();
    }, [id]);

    useEffect(() => {
        Object.keys(estate).forEach((key: keyof Estate) =>
            setValue(key, estate[key])
        );
    }, [estate]);

    return (
        <Paper>
            <EstateMap
                onLocationChange={(location) =>
                    setEstate((prev) => ({
                        ...prev,
                        lat: location.lat,
                        lon: location.lng,
                    }))
                }
                locations={[estate]}
            />
            <EstateDetailsTemplate
                users={realtors}
                handleSubmit={handleSubmit(onSubmit)}
                errors={errors as SchemaErrors}
                register={register}
                control={control}
                goBack={goBack}
                onDelete={deleteEstate}
            />
        </Paper>
    );
};

import React, { SyntheticEvent, useCallback, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { LoginTemplate } from '../components/Templates/LoginTemplate';
import { SchemaErrors } from '../types/SchemaErrors';
import { login, setToken, resetToken } from '../api/auth';
import { RouteComponentProps } from 'react-router-dom';
import { LoginSchema, LoginValues } from '../forms/login';

interface Props extends RouteComponentProps {
    onSuccess: () => void;
    logout: () => void;
}

const Login = (props: Props): React.ReactElement => {
    const { register, handleSubmit, errors, setError } = useForm({
        validationSchema: LoginSchema,
    });

    // logout existing user when it gets to login
    useEffect(props.logout, []);

    const setTokenAndRedirect = useCallback((token: string): void => {
        setToken(token);
        props.onSuccess();
        props.history.push('/dashboard');
    }, []);
    const onSubmit = async (
        data: LoginValues,
        e: SyntheticEvent
    ): Promise<void> => {
        e.preventDefault();

        try {
            const token = await login(data.email, data.password);

            setTokenAndRedirect(token);
        } catch (err) {
            setError('password', 'invalidLogin', 'Login failed.');
        }
    };

    return (
        <LoginTemplate
            title="Login"
            handleSubmit={handleSubmit(onSubmit)}
            errors={errors as SchemaErrors}
            emailRef={register}
            passwordRef={register}
        />
    );
};

export default Login;

import React from 'react';
import clsx from 'clsx';
import {
    CssBaseline,
    Drawer,
    Box,
    List,
    Divider,
    IconButton,
    Container,
    Grid,
} from '@material-ui/core';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { Copyright } from '../../components/Copyright';
import { Header } from '../../components/Header';
import { LeftMenu } from '../../components/LeftMenu';
import { Routes } from './Routes';
import { BrowserRouter as Router } from 'react-router-dom';
import { useStyles } from './style';

export const Dashboard = (): React.ReactElement => {
    const classes = useStyles({});
    const [open, setOpen] = React.useState(false);
    const handleDrawerOpen = (): void => setOpen(true);
    const handleDrawerClose = (): void => setOpen(false);

    return (
        <Router>
            <div className={classes.root}>
                <CssBaseline />
                <Header open={open} onClick={handleDrawerOpen} />
                <Drawer
                    variant="permanent"
                    classes={{
                        paper: clsx(
                            classes.drawerPaper,
                            !open && classes.drawerPaperClose
                        ),
                    }}
                    open={open}
                >
                    <div className={classes.toolbarIcon}>
                        <IconButton onClick={handleDrawerClose}>
                            <ChevronLeftIcon />
                        </IconButton>
                    </div>
                    <Divider />
                    <List>
                        <LeftMenu />
                    </List>
                </Drawer>
                <main className={classes.content}>
                    <div className={classes.appBarSpacer} />
                    <Container maxWidth="lg" className={classes.container}>
                        <Grid container spacing={3}>
                            <Grid item xs={12}>
                                <Routes />
                            </Grid>
                        </Grid>
                        <Box pt={4}>
                            <Copyright />
                        </Box>
                    </Container>
                </main>
            </div>
        </Router>
    );
};

import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { EstateList } from '../EstateList';
import { EstateDetails } from '../EstateDetails';
import { UserList } from '../UserList';
import { UserDetails } from '../UserDetails';

export const Routes = (): React.ReactElement => {
    return (
        <Switch>
            <Route path="/dashboard/users" component={UserList} />
            <Route path="/dashboard/user/:id" component={UserDetails} />
            <Route path="/dashboard/estate/:id" component={EstateDetails} />
            <Route component={EstateList} />
        </Switch>
    );
};

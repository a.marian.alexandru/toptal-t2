import React, { useCallback, useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { RouteComponentProps } from 'react-router-dom';
import { SchemaErrors, User } from '../../types';
import * as api from '../../api/user';
import { UserDetailsTemplate } from '../../components/Templates/UserDetails';

export const UserDetails = (
    props: RouteComponentProps<{ id: string }>
): React.ReactElement => {
    const { id } = props.match.params;
    const {
        control,
        register,
        handleSubmit,
        errors,
        setError,
        setValue,
    } = useForm({});
    const onSubmit = useCallback(
        (user: User) => {
            (async () => {
                if (id === 'new') {
                    const response = await api.create(user);

                    props.history.push('/dashboard/user/' + response.id);
                } else {
                    await api.update({ ...user, id: parseInt(id) });
                }
            })();
        },
        [id]
    );
    const goBack = () => props.history.push('/dashboard/users');

    const remove = async () => {
        if (Number.isInteger(Number(id))) {
            await api.remove(parseInt(id));
        }

        goBack();
    };

    useEffect(() => {
        (async () => {
            if (id === 'new') return;

            const user = await api.getById(parseInt(id));

            Object.keys(user).forEach((key: keyof User) =>
                setValue(key, user[key])
            );
        })();
    }, [id]);

    return (
        <UserDetailsTemplate
            handleSubmit={handleSubmit(onSubmit)}
            errors={errors as SchemaErrors}
            register={register}
            control={control}
            goBack={goBack}
            onDelete={remove}
        />
    );
};

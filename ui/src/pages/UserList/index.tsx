import React, { useEffect, useState, useCallback } from 'react';
import * as api from '../../api/user';
import { Paper } from '../../components/Paper';
import { List } from '../../components/List';
import { User } from '../../types';
import { RouteComponentProps } from 'react-router-dom';
import { Button } from '@material-ui/core';

const header: Partial<Record<keyof User, string>> = {
    id: 'User ID',
    email: 'Email',
    role: 'Role',
};

export const UserList = (props: RouteComponentProps): React.ReactElement => {
    const [list, setList] = useState<Array<User>>([]);

    useEffect(() => {
        (async (): Promise<void> => {
            setList(await api.get());
        })();
    }, []);

    const renderActions = useCallback(
        () => (
            <Button
                color="primary"
                variant="outlined"
                onClick={() => props.history.push('/dashboard/user/new')}
            >
                Create
            </Button>
        ),
        []
    );

    const redirectToDetails = (el: User) => {
        props.history.push('/dashboard/user/' + el.id);
    };

    return (
        <Paper>
            <List
                onRowClick={redirectToDetails}
                title="Users"
                renderActions={renderActions}
                header={header}
                rows={list}
            />
        </Paper>
    );
};

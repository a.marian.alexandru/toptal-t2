/* eslint-disable @typescript-eslint/camelcase */
import React from 'react';
import { User } from '../types';

export const emptyUser: Readonly<User> = {
    id: 0,
    email: null,
    password: null,
    role: null,
};

export const UserContext = React.createContext(emptyUser);

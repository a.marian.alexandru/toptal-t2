import axios from 'axios';
import { baseUrl } from './config';
import { User } from '../types';

export const fetchLoggedUser = async (): Promise<User> => {
    const response = await axios.get(baseUrl + '/auth/user');

    return response.data;
};

export const signup = async (email: string, password: string): Promise<string> => {
    const response = await axios.post(baseUrl + '/auth/signup', {
        email,
        password,
    });

    return response.data;
};

export const login = async (email: string, password: string): Promise<string> => {
    const response = await axios.post(baseUrl + '/auth/login', {
        email,
        password,
    });

    return response.data;
};

let token: string = null;

export const setToken = (authorization: string): void => {
    token = authorization;

    if (token) axios.defaults.headers['Authorization'] = 'Bearer ' + token;
    else delete axios.defaults.headers['Authorization'];

    if (process.env.NODE_ENV === 'development') {
        if (token) localStorage.setItem('token', token);
        else localStorage.removeItem('token');
    }
};

export const resetToken = (): void => {
    setToken(null);
};

export const getToken = (): string => {
    if (!token && process.env.NODE_ENV === 'development') {
        const devToken = localStorage.getItem('token');

        if (devToken) setToken(devToken);

        return devToken;
    }

    return token;
};

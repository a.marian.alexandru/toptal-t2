import axios from 'axios';
import { baseUrl } from './config';
import { User } from '../types';

const apiUrl = baseUrl + '/user';

export const get = async (): Promise<User[]> => {
    const response = await axios.get(apiUrl);

    return response.data;
};

export const getById = async (id: number): Promise<User> => {
    const response = await axios.get(`${apiUrl}/${id}`);

    return response.data;
};

export const getRealtors = async (): Promise<User[]> => {
    const response = await axios.get(`${apiUrl}?role=REALTOR`);

    return response.data;
}


export const update = async (user: User): Promise<void> => {
    const { id, ...body } = user;

    await axios.put(`${apiUrl}/${id}`, body);
}

export const create = async (user: User): Promise<{ id: number }> => {
    const { id, ...body } = user;

    const response = await axios.post(apiUrl, body);

    return response.data;
}

export const remove = async (id: number): Promise<void> => {
    await axios.delete(`${apiUrl}/${id}`);
};


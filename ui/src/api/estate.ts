import axios from 'axios';
import qs from 'qs';
import { baseUrl } from './config';
import { Estate, EstateState } from '../types';

const apiUrl = baseUrl + '/estate';
export type AvailableFilter = Partial<{
    sizeFrom: number,
    sizeTo: number,
    priceFrom: number,
    priceTo: number,
    roomsFrom: number,
    roomsTo: number,
    state: EstateState
}>;

export const get = async (filter: AvailableFilter = {}): Promise<Estate[]> => {
    const query = qs.stringify(filter);
    const response = await axios.get(`${apiUrl}?${query}`);

    return response.data;
};

export const getById = async (id: number): Promise<Estate> => {
    const response = await axios.get(`${apiUrl}/${id}`);

    return response.data;
};

export const update = async (estate: Estate): Promise<void> => {
    const { id, add_date, ...body } = estate;

    await axios.put(`${apiUrl}/${id}`, body);
}

export const create = async (estate: Estate): Promise<{ id: number }> => {
    const { id, add_date, ...body } = estate;

    const response = await axios.post(apiUrl, body);

    return response.data;
}

export const remove = async (id: number): Promise<void> => {
    await axios.delete(`${apiUrl}/${id}`);
};

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackRootPlugin = require('html-webpack-root-plugin');

module.exports = (env, args) => ({
    entry: path.resolve(__dirname, 'src/index.tsx'),

    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].[hash].js',
        publicPath: '/',
    },

    optimization: {
        splitChunks: {
            chunks: 'all',
        },
    },

    plugins: [
        new webpack.DefinePlugin({
            'process.env.API_URL': args.api
                ? `"${args.api}"`
                : '"http://localhost:5001"',
        }),
        new HtmlWebpackPlugin({ title: 'Rentals' }),
        new HtmlWebpackRootPlugin(),
    ],

    // Enable sourcemaps for debugging webpack's output.
    devtool: args.mode === 'production' ? 'hidden-source-map' : 'source-map',

    resolve: {
        extensions: ['.js', '.ts', '.tsx'],
    },

    module: {
        rules: [
            {
                test: /\.ts(x?)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'ts-loader',
                    },
                ],
            },
            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            {
                enforce: 'pre',
                test: /\.js$/,
                loader: 'source-map-loader',
            },
        ],
    },

    devServer: {
        historyApiFallback: true,
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        host: '0.0.0.0',
        port: 5000,
        watchOptions: { ignored: /node_modules/ },
    },
});

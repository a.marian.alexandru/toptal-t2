# ************************************************************
# Sequel Pro SQL dump
# Version 5446
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.25)
# Database: rentals
# Generation Time: 2020-06-01 16:13:04 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table estate
# ------------------------------------------------------------

DROP TABLE IF EXISTS `estate`;

CREATE TABLE `estate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `add_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `realtor` int(11) unsigned NOT NULL,
  `name` tinytext,
  `description` text,
  `size` float DEFAULT NULL,
  `rooms` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `lat` decimal(7,4) DEFAULT NULL,
  `lon` decimal(7,4) DEFAULT NULL,
  `state` enum('AVAILABLE','RENTED') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_apartment_user` (`realtor`),
  CONSTRAINT `fk_apartment_user` FOREIGN KEY (`realtor`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `estate` WRITE;
/*!40000 ALTER TABLE `estate` DISABLE KEYS */;

INSERT INTO `estate` (`id`, `add_date`, `realtor`, `name`, `description`, `size`, `rooms`, `price`, `lat`, `lon`, `state`)
VALUES
	(1,'2020-06-01 14:24:52',3,'Apartment 1','very very long description',50.4,2,200,3.1231,2.1231,'AVAILABLE'),
	(2,'2020-06-01 14:26:05',3,'Apartment 22','very very long description',65,2,200,3.1231,2.1231,'RENTED'),
	(3,'2020-06-01 14:33:02',3,'Apartment 3','very very long description',50.4,2,200,3.1231,2.1231,'AVAILABLE'),
	(6,'2020-06-01 15:42:26',4,'Apartment 6','very very long description',50.4,2,200,3.1231,2.1231,'AVAILABLE'),
	(7,'2020-06-01 16:04:33',3,'Apartment 6','very very long description',50.4,2,200,3.1231,2.1231,'AVAILABLE');

/*!40000 ALTER TABLE `estate` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` tinytext NOT NULL,
  `password` tinytext NOT NULL,
  `role` enum('ADMIN','REALTOR','USER') NOT NULL DEFAULT 'USER',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`(255))
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

-- Passwords: user, realtor, admin
INSERT INTO `user` (`id`, `email`, `password`, `role`)
VALUES
	(1,'user@user.com','$2a$10$MZIhR1Hwely3IJW1vIk5RuSrmOIVV3xIUKBhDfVLWVhRvMyV4zsL6','USER'),
	(3,'realtor@realtor.com','$2a$10$T.AQV1VTOupYTILr/yNfqePg.pEBkM2r5vkM/dpKYS121jwMEixMu','REALTOR'),
	(4,'admin@admin.com','$2a$10$IQPif0kwTOf0bLOkXdk4f.HJxf4myd4TsmPaOQwq8Na.4vD8uIHeq','ADMIN');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

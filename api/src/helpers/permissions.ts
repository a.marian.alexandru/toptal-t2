import { RequestHandler } from "express";
import { AuthResponse } from "./auth";
import { get } from "./db/user";
import { UserRole } from "../types";

// authMiddleware needs to run before
export const restrictUser = (userRole: UserRole, allowedMethods: string[]): RequestHandler => async (req, res: AuthResponse, next) => {
    const user = (await get({ id: res.locals.userId }))[0];

    if (user.role === userRole && !allowedMethods.includes(req.method)) {
        return res.status(405).send('Operation is not allowed');
    }

    return next();
};
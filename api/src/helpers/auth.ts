import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { RequestHandler, Response } from 'express';
import { User } from '../types';
import { create, get } from './db/user';
import { HttpError } from './error';
import { tokenSecret } from '../config';
import { logger } from './log';

export const safeUser = (user: User): User => ({
    ...user,
    password: ''
});

export const createToken = (userId: number): string =>
    jwt.sign({ userId }, tokenSecret);

export const signup = async (user: User): Promise<string> => {
    const password = await bcrypt.hash(user.password, 10);

    const users = await get({ email: user.email });

    if (users.length) {
        throw new HttpError(`There is already an user with this email.`);
    }

    const userId = await create({ ...user, password });

    return createToken(userId);
};

export const login = async ({
    email,
    password,
}: {
    email: string;
    password: string;
}): Promise<string> => {
    const users = await get({ email });

    if (!users.length) {
        throw new HttpError(`No such user found for email: ${email}`);
    }

    const valid = await bcrypt.compare(password, users[0].password);

    if (!valid) {
        throw new HttpError('Invalid password');
    }

    return createToken(users[0].id);
};

export interface AuthResponse extends Response {
    locals: {
        userId: number;
    };
}

export const parseAuthorisation = (authorization: string): number => {
    try {
        if (authorization) {
            const token = authorization.replace('Bearer ', '');
            const { userId } = jwt.verify(token, tokenSecret) as {
                userId: string;
            };

            return parseInt(userId);
        }
    } catch (err) {
        logger.error(err);
    }

    throw new HttpError('Missing or invalid authorization', 403);
};

export const authMiddleware: RequestHandler = (req, res, next) => {
    const authorization = req.get('Authorization');

    res.locals.userId = parseAuthorisation(authorization);

    return next();
};

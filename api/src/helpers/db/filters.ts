export const parseRequestFilters = (requestQuery: Record<string, string>, filterKeys: string[]) => {
    return filterKeys.reduce((acc: {}, key) => {
        const from = requestQuery[`${key}From`] || '';
        const to = requestQuery[`${key}To`] || '';
        const exactValue = requestQuery[key] as string;

        if (!from && !to && !exactValue) return acc;

        if (exactValue) {
            return {
                ...acc,
                [key]: exactValue
            }
        }

        return {
            ...acc,
            [key]: {
                from,
                to
            }
        }
    }, {});
}
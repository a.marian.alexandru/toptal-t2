import { User } from '../../types';
import { CRUD } from './crud';

type CreateUserType = Omit<User, 'id'>;

const editableFields: Array<keyof User> = ['email', 'password', 'role'];
const filterableFields: Array<keyof User> = ['id', 'email', 'password', 'role'];
const table = 'user';

const { create, get, remove, update } = new CRUD<CreateUserType, User>({ table, editableFields, filterableFields });

export {
    create,
    get,
    remove,
    update
};

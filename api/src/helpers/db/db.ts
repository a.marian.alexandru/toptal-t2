import mysql from 'mysql';
import { dbConfig, debugSQL } from '../../config';
import { logger } from '../log';
import { Between, isBetween } from '../../types/Db';

const mysqlDb = mysql.createConnection(dbConfig);

export const queryDB = (
    sql: string,
    args?: Array<any>,
): Promise<Array<unknown> | number | true> =>
    new Promise((resolve, reject) => {
        if (debugSQL) {
            logger.debug('SQL', sql, args);
        }

        mysqlDb.query(sql, args, (err, rows) => {
            if (err) {
                return reject(err);
            }
            if (rows.insertId) {
                return resolve(rows.insertId);
            }
            if (rows.changedRows || rows.affectedRows) {
                return resolve(true);
            }

            return resolve(rows);
        });
    });
const exitError = (err: Error): void => {
    logger.error(err);
    process.exit();
};

if (process.env.ENV !== 'test') {
    mysqlDb.connect({ multipleStatements: true }, async err => {
        if (err) {
            exitError(err);
        }

        try {
            const tables = (await queryDB('SHOW TABLES')) as Array<object>;

            logger.info(tables, 'Found tables');
        } catch (err) {
            exitError(err);
        }
    });
}


export const addPlaceholder = (fields: string[], operator = 'and'): string => {
    return fields.map(key => `\`${key}\` = ?`).join(` ${operator} `);
}

export const addExpandedPlaceholder = (expandedFilter: Record<string, Between | unknown>): string => {
    return Object.keys(expandedFilter)
        .flatMap(key => {
            const val = expandedFilter[key];
            if (isBetween(val)) {
                const { from, to } = val as Between;
                const result = [];

                if (from) result.push(`\`${key}\` >= ?`);
                if (to) result.push(`\`${key}\` <= ?`);

                return result;
            }

            return `\`${key}\` = ?`;
        })
        .join(' and ');
};

export const splitSearch = (search: string) => search.split('<');

export const escapeFields = (fields: string[]): string => fields.map(el => '`' + el + '`').join(',');

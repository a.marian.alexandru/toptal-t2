import { queryDB, escapeFields, addPlaceholder, addExpandedPlaceholder, splitSearch } from './db';
import { Between, isBetween } from '../../types/Db';

interface Params<T> {
    table: string;
    editableFields: Array<Extract<keyof T, string>>;
    filterableFields: Array<Extract<keyof T, string>>;
}

export class CRUD<T extends Partial<U>, U> {
    params: Params<U>;

    constructor(params: Params<U>) {
        this.params = params;
    }

    create = async (resource: T): Promise<number> => {
        const fields = escapeFields(this.params.editableFields);
        const values = this.params.editableFields.map(field => resource[field]);

        const id = (await queryDB(
            'insert into `' + this.params.table + '`(' + fields + ') values ?',
            [[values]],
        )) as number;

        return id;
    }


    remove = async (resourceId: number): Promise<void> => {
        await queryDB('delete from `' + this.params.table + '` where `id` = ?', [resourceId]);
    }

    update = async (
        resourceId: number,
        body: { [key: string]: string | number | boolean },
    ): Promise<void> => {
        const fields = this.params.editableFields.filter(key => key in body);

        if (!fields.length) {
            throw new Error('Nothing to update');
        }

        const keysWithPlaceholder = addPlaceholder(fields, ',');
        const values = fields.map(key => body[key]);
        const query = 'update `' + this.params.table + '` set ' + keysWithPlaceholder + ' where `id` = ?';

        await queryDB(
            query,
            [...values, resourceId],
        );
    }

    // { size: '20<30' } <- input
    // { size: { from: x, to: y } } <- expandedFilter
    // where size >= ? and size <= ? <- keysWithPlaceholder
    // values = [20, 30] <- values
    // where size >= 20 and size <= 30 <- result

    get = async (filter: { [P in keyof U]?: U[P] | Between }, limit: number = 50, offset: number = 0): Promise<U[]> => {
        const fields = this.params.filterableFields.filter(key => key in filter);
        const values = Object.values(filter).flatMap(val => {
            if (isBetween(val)) return [val.from, val.to];

            return val;
        }).filter(el => el !== '');

        let where = '';
        if (fields.length > 0) {
            const keysWithPlaceholder = addExpandedPlaceholder(filter);

            where = `where ${keysWithPlaceholder}`;
        }

        const query = 'select * from `' + this.params.table + '` ' + where + ` limit ${limit} offset ${offset}`;

        const result = (await queryDB(query, values)) as Array<U>;

        return result;
    }

};

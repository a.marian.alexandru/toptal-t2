import { Estate } from '../../types';
import { CRUD } from './crud';

type StrictEstate = Omit<Estate, 'add_date' | 'id'>;

const editableFields: Array<keyof Estate> = ['realtor', 'name', 'description', 'size', 'rooms', 'price', 'lat', 'lon', 'state'];
const filterableFields: Array<keyof Estate> = ['id', 'size', 'price', 'rooms', 'state'];
const table = 'estate';

const { create, get, remove, update } = new CRUD<StrictEstate, Estate>({ table, editableFields, filterableFields });

export {
    create,
    get,
    remove,
    update
};

import { Router } from 'express';
import Joi from '@hapi/joi';
import { createValidator } from 'express-joi-validation';
import * as bcrypt from 'bcryptjs';
import { authMiddleware, AuthResponse, safeUser } from '../helpers/auth';
import { restrictUser } from '../helpers/permissions';
import * as db from '../helpers/db/user';
import { User } from '../types';

const router = Router();
const restrictUserActions = restrictUser('USER', ['GET']);
const restrictRealtorActions = restrictUser('REALTOR', ['GET']);

router.use(authMiddleware);
router.use(restrictUserActions);
router.use(restrictRealtorActions);

const validator = createValidator()

const roleSchema = Joi.string().valid('ADMIN' , 'REALTOR' , 'USER');

const userSchema: Record<string, Joi.Schema> = {
    email: Joi.string().email(),
    password: Joi.string(),
    role: roleSchema
};

const allFieldsRequired = Joi.object(
    Object.keys(userSchema).reduce((acc, key) => ({
        ...acc,
        [key]: userSchema[key].required()
    }), {})
);

router.post('/', validator.body(allFieldsRequired), async (req, res: AuthResponse, next) => {
    try {
        const user: Omit<User, 'id'> = req.body;

        user.password = await bcrypt.hash(user.password, 10);

        const id = await db.create(user);

        res.send({ id });
    } catch (err) {
        next(err);
    }
});

const paramsSchema = Joi.object({
    id: Joi.number().required()
});

router.put('/:id', validator.params(paramsSchema), validator.body(Joi.object(userSchema)), async (req, res: AuthResponse, next) => {
    try {
        const user: Partial<Omit<User, 'id'>> = req.body;

        if(user.password) {
            user.password = await bcrypt.hash(user.password, 10);
        }

        await db.update(parseInt(req.params.id), user);

        res.send();
    } catch (err) {
        next(err);
    }
});

router.delete('/:id', validator.params(paramsSchema), async (req, res: AuthResponse, next) => {
    try {
        await db.remove(parseInt(req.params.id));

        res.send();
    } catch (err) {
        next(err);
    }
});

router.get('/:id', validator.params(paramsSchema), async (req, res: AuthResponse, next) => {
    try {
        const id = parseInt(req.params.id);
        const result = await db.get({ id });

        res.send(safeUser(result[0]));
    } catch (err) {
        next(err);
    }
});

const filterSchema = Joi.object({
    email: Joi.string().email(),
    role: roleSchema
});

router.get('/', validator.query(filterSchema), async (req, res: AuthResponse, next) => {
    try {
        const result = await db.get(req.query);

        res.send(result.map(safeUser));
    } catch (err) {
        next(err);
    }
});

export default router;

import { Router } from 'express';
import authRouter from './auth';
import estateRouter from './estate';
import userRouter from './user';

const routes: Record<string, Router> = {
    '/auth': authRouter,
    '/estate': estateRouter,
    '/user': userRouter,
};

export default routes;

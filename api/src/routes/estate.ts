import { Router } from 'express';
import Joi from '@hapi/joi';
import { createValidator } from 'express-joi-validation';
import { authMiddleware, AuthResponse } from '../helpers/auth';
import { restrictUser } from '../helpers/permissions';
import * as db from '../helpers/db/estate';
import { Estate } from '../types';
import { parseRequestFilters } from '../helpers/db/filters';

const router = Router();
const restrictUserToView = restrictUser('USER', ['GET', 'OPTIONS']);

router.use(authMiddleware);
router.use(restrictUserToView);

const validator = createValidator()

const stateSchema = Joi.string().valid('AVAILABLE', 'RENTED');

const estateSchema: Record<string, Joi.Schema> = {
    name: Joi.string(),
    description: Joi.string(),
    realtor: Joi.number(),
    size: Joi.number(),
    rooms: Joi.number(),
    price: Joi.number(),
    lat: Joi.number(),
    lon: Joi.number(),
    state: stateSchema
};

const allFieldsRequired = Joi.object(
    Object.keys(estateSchema).reduce((acc, key) => ({
        ...acc,
        [key]: estateSchema[key].required()
    }), {})
);

router.post('/', validator.body(allFieldsRequired), async (req, res: AuthResponse, next) => {
    try {
        const estate: Omit<Estate, 'add_date' | 'id'> = {
            ...req.body,
            realtor: req.body.realtor || res.locals.userId
        };

        const id = await db.create(estate);

        res.send({ id });
    } catch (err) {
        next(err);
    }
});

const paramsSchema = Joi.object({
    id: Joi.number().required()
});

router.put('/:id', validator.params(paramsSchema), validator.body(Joi.object(estateSchema)), async (req, res: AuthResponse, next) => {
    try {
        const estate: Omit<Estate, 'add_date' | 'id'> = {
            ...req.body,
            realtor: req.body.realtor || res.locals.userId
        };

        await db.update(parseInt(req.params.id), estate);

        res.send();
    } catch (err) {
        next(err);
    }
});

router.delete('/:id', validator.params(paramsSchema), async (req, res: AuthResponse, next) => {
    try {
        await db.remove(parseInt(req.params.id));

        res.send();
    } catch (err) {
        next(err);
    }
});

router.get('/:id', validator.params(paramsSchema), async (req, res: AuthResponse, next) => {
    try {
        const id = parseInt(req.params.id);
        const result = await db.get({ id });

        res.send(result[0]);
    } catch (err) {
        next(err);
    }
});

const filterSchema = Joi.object({
    sizeFrom: Joi.number(),
    sizeTo: Joi.number(),
    priceFrom: Joi.number(),
    priceTo: Joi.number(),
    roomsFrom: Joi.number(),
    roomsTo: Joi.number(),
    state: stateSchema,
    limit: Joi.number().max(500),
    offset: Joi.number()
});

router.get('/', validator.query(filterSchema), async (req, res: AuthResponse, next) => {
    try {
        const filterKeys = ['size', 'price', 'rooms', 'state'];
        const filter = parseRequestFilters(req.query as Record<string, string>, filterKeys);

        const { limit, offset } = req.query;

        const result = await db.get(filter, limit as unknown as number, offset as unknown as number);

        res.send(result);
    } catch (err) {
        next(err);
    }
});

export default router;

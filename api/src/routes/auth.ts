import { Router } from 'express';
import Joi from '@hapi/joi';
import { createValidator } from 'express-joi-validation';
import { login, signup, authMiddleware, AuthResponse, safeUser } from '../helpers/auth';
import { User } from '../types';
import { get } from '../helpers/db/user';
import { HttpError } from '../helpers/error';

const router = Router();

const validator = createValidator();

const userSchema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().min(4).required()
})

router.post('/signup', validator.body(userSchema), async (req, res, next) => {
    const user: User = {
        id: 0,
        email: req.body.email,
        password: req.body.password,
        role: 'USER'
    };

    try {
        const response = await signup(user);

        res.send(response);
    } catch (err) {
        next(err);
    }
});

router.post('/login', validator.body(userSchema), async (req, res, next) => {
    const { email, password } = req.body;

    try {
        const response = await login({ email, password });

        res.send(response);
    } catch (err) {
        next(err);
    }
});

router.get('/user', authMiddleware, async (_req, res: AuthResponse, next) => {
    try {
        const users = await get({ id: res.locals.userId });

        if (!users.length) {
            throw new HttpError("User not logged", 400);
        }

        const user = safeUser(users[0]);

        res.send(user);
    } catch (err) {
        next(err);
    }
});

export default router;

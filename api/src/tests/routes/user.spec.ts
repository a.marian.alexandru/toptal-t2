import request from 'supertest';
import * as config from '../../config';
import { app } from '../../index';
import * as mysql from '../../helpers/db/db';
import { User } from '../../types';
import { safeUser } from '../../helpers/auth';

const readOnlyUsers: User[] = [{
    id: 1,
    email: 'user@user.com',
    password: 'user',
    role: 'USER'
}, {
    id: 2,
    email: 'realtor@realtor.com',
    password: 'realtor',
    role: 'REALTOR'
}, {
    id: 3,
    email: 'admin@admin.com',
    password: 'admin',
    role: 'ADMIN'
}];

jest.mock('mysql');

describe('CRUD Users', () => {
    let token = null;
    const authHeader = { 'Authorization': '' };
    let users: User[] = [];

    beforeAll(done => {
        // @ts-ignore
        config.tokenSecret = 'secret';

        jest.spyOn(mysql, 'queryDB').mockReturnValue(Promise.resolve([]));

        request(app)
            .post('/auth/signup')
            .send({
                email: readOnlyUsers[0].email,
                password: readOnlyUsers[0].password
            })
            .expect(200, (_err, res) => {
                token = res.text;
                authHeader.Authorization = `Bearer ${token}`;

                done();
            });
    })
    beforeEach(() => {
        users = JSON.parse(JSON.stringify(readOnlyUsers));

        jest.spyOn(mysql, 'queryDB').mockReturnValue(
            Promise.resolve(users)
        );
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should throw 403 if token is missing', async () => {
        const res = await request(app).get('/user');

        expect(res.status).toEqual(403);
    })

    it('should allow users to see all users', async () => {
        const res = await request(app).get('/user').set(authHeader);

        expect(res.status).toEqual(200);
        expect(res.body).toEqual(readOnlyUsers.map(safeUser));
    });

    it('should forbit users to create other users', async () => {
        const res = await request(app).post('/user').set(authHeader);

        expect(res.status).toEqual(405);
    });

    it('should prevent admins from entering invalid emails', async () => {
        users[0].role = 'ADMIN';

        const res = await request(app).put('/user/2').set(authHeader).send({ email: 'invalid' });

        expect(res.status).toEqual(400);
    });

    it('should allow admins to update other users', async () => {
        users[0].role = 'ADMIN';

        const res = await request(app).put('/user/2').set(authHeader).send({ email: 'ok@ok.com' });

        expect(res.status).toEqual(200);
    });
})
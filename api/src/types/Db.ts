export type Between = {
    from: number;
    to: number;
};

export const isBetween = (value: any): value is Between => {
    if (typeof value !== 'object') return false;

    return 'from' in value && 'to' in value;
}

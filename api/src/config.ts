export const dbConfig = {
    database: process.env.MYSQL_DATABASE,
    host: process.env.MYSQL_HOST,
    password: process.env.MYSQL_ROOT_PASSWORD,
    user: process.env.MYSQL_USER,
};
export const tokenSecret = process.env.TOKEN_SECRET;
export const debugSQL = process.env.DEBUG_SQL;

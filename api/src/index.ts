import express, { Request, Response, NextFunction } from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import pino from 'pino-http';
import { HttpError } from './helpers/error';
import routes from './routes';
import { logger } from './helpers/log';

export const app = express();

// Express setup
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(pino({ logger: logger }));

// Basic routes
app.get('/', (_req, res) => res.send('Rentals API'));

// Load all routes
Object.keys(routes).forEach(route => app.use(route, routes[route]));

// Error handling
// eslint-disable-next-line @typescript-eslint/no-unused-vars
app.use((err: Error, req: Request, res: Response, _next: NextFunction) => {
    req.log.error(err);
    if (err instanceof HttpError) {
        res.status(err.statusCode).send(err.message);
    } else {
        res.status(500).send('Internal error');
    }
});

// Server startup
const port = process.env.PORT || '3001';

if (process.env.ENV !== 'test') {
    app.listen(parseInt(port), '0.0.0.0');
    logger.info('Rentals API running at localhost:' + port);
}
